package test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.spi.HttpServerProvider;

class BussinessException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 451443969274094522L;

	public BussinessException() {
		super();
	}

	public BussinessException(String s) {
		super(s);
	}

	public BussinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BussinessException(Throwable cause) {
		super(cause);
	}
}

abstract class AbstractHttpHandler implements HttpHandler {
	protected Map<String, String> parametersMap = null;
	protected String requestBodyStr = null;
	private HttpExchange httpExchange = null;
	private static final String ENCODES = "UTF-8";
	private static final String EMPTY_STR = "";
	static final int DEFAULT_INITIAL_CAPACITY = 1 << 4;
	static final float DEFAULT_LOAD_FACTOR = 0.75f;

	public static String readInputStream(InputStream is) {
		try {
			int requestBodySize = is.available();
			if (requestBodySize > 0) {
				byte[] ba = new byte[requestBodySize];
				is.read(ba);
				return new String(ba, ENCODES);
			}
		} catch (IOException e) {
			System.err.println("read InputStream throw exception:\n");
			e.printStackTrace();
		}
		return EMPTY_STR;
	}

	public static Map<String, String> readQueryParameters(HttpExchange httpExchange) {
		Map<String, String> parameters = null;

		String queryParameters = httpExchange.getRequestURI().getQuery();

		if (queryParameters != null && queryParameters.trim().length() > 0) {
			String[] parameterArray = queryParameters.split("&");
			if (parameterArray.length > 16) {
				parameters = new HashMap<String, String>((int) Math.ceil(parameterArray.length / DEFAULT_LOAD_FACTOR));
			} else if (parameterArray.length > 0) {
				parameters = new HashMap<String, String>();
			} else {
				return parameters;
			}

			for (String paramString : parameterArray) {
				String[] pa = paramString.split("=");
				if (pa.length == 2) {
					parameters.put(pa[0], pa[1]);
				}
			}
		}

		return parameters;
	}

	public void handle(HttpExchange httpExchange) throws IOException {
		parametersMap = readQueryParameters(httpExchange);
		System.out.printf("query parameters: %s\n", parametersMap != null ? parametersMap.toString() : null);

		InputStream requestBody = httpExchange.getRequestBody();

		requestBodyStr = readInputStream(requestBody);
		System.out.printf("request body Start=====\n%s\nrequest body End=====\n", requestBodyStr);

		this.httpExchange = httpExchange;

		try {
			String responseBody = deal();

			success(responseBody);
		} catch (BussinessException e) {
			e.printStackTrace();
			error(e.getMessage());
			return;
		}
	}

	public abstract String deal() throws BussinessException;

	protected void success(String successResponseBody) throws IOException {
		sendResponse(successResponseBody, 200);
	}

	protected void error(String errorResponseBody) throws IOException {
		sendResponse(errorResponseBody, 500);
	}

	protected void sendResponse(String responseBody, int httpStatus) throws IOException {
		if (responseBody == null) {
			responseBody = "";
		}

		httpExchange.sendResponseHeaders(httpStatus, responseBody.length());
		OutputStream out = httpExchange.getResponseBody();
		out.write(responseBody.getBytes(ENCODES));
		out.close();
	}

}

class AddHttpHandler extends AbstractHttpHandler {

	@Override
	public String deal() throws BussinessException {
		BigDecimal result = new BigDecimal("0");
		// add query parameters
		if (parametersMap != null) {
			Set<Entry<String, String>> set = parametersMap.entrySet();
			for (Entry<String, String> entry : set) {
				String value = entry.getValue();
				try {
					BigDecimal v = new BigDecimal(value);
					result = result.add(v);
				} catch (NumberFormatException e) {
					String msg = String.format("[%s=%s] is not a number", entry.getKey(), entry.getValue());
					throw new BussinessException(msg, e);
				}
			}
		} else {
			throw new BussinessException("numbers can not be null");
		}

		return result.toPlainString();
	}

}

class MultHttpHandler extends AbstractHttpHandler {

	@Override
	public String deal() throws BussinessException {
		BigDecimal result = new BigDecimal("1");
		// mult query parameters
		if (parametersMap != null) {
			Set<Entry<String, String>> set = parametersMap.entrySet();
			for (Entry<String, String> entry : set) {
				String value = entry.getValue();
				try {
					BigDecimal v = new BigDecimal(value);
					result = result.multiply(v);
				} catch (NumberFormatException e) {
					String msg = String.format("[%s=%s] is not a number", entry.getKey(), entry.getValue());
					throw new BussinessException(msg, e);
				}
			}
		} else {
			throw new BussinessException("numbers can not be null");
		}

		return result.toPlainString();
	}

}

public class HttpTest {

	public static void main(String[] args) throws Exception {
		HttpServerProvider httpServerProvider = HttpServerProvider.provider();
		InetSocketAddress addr = new InetSocketAddress(80);
		HttpServer httpServer = httpServerProvider.createHttpServer(addr, 2);
		httpServer.createContext("/add", new AddHttpHandler());
		httpServer.createContext("/mult", new MultHttpHandler());
		httpServer.setExecutor(null);
		httpServer.start();
		System.out.println("http sever started");
	}
}
